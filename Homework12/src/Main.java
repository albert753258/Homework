import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int array[] = fillArray();
        bubbleSort(array);
    }
    public static void bubbleSort(int array[]){
        boolean sorted = false;
        int j;
        while (!sorted){
            sorted = true;
            for (int i = 0; i < array.length - 1; i ++){
                if (array[i] < array[i + 1]){
                    j = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = j;
                    sorted = false;
                }
            }
        }
        System.out.println(array[0]);
    }
    public static int[] fillArray(){
        int array[] = new int[1000];
        Random random = new Random();
        for(int i = 0; i < array.length; i ++){
            array[i] = random.nextInt(200);
        }
        return array;
    }
}
