public class Program {
    public static void main(String[] args) {
        int sum = 0;
        int num = 479598;
        for(int i = 0; i < 6; i++){
            sum += num % 10;
            num = num / 10;
        }
        System.out.println(sum);
    }
}
