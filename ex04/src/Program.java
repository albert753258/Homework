import sun.reflect.generics.reflectiveObjects.GenericArrayTypeImpl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Elem[] codes = new Elem[65535];
        for(int i = 0; i < codes.length; i++){
            codes[i] = new Elem();
        }
        String input = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASSSSSSSSSSSSSSSSSSSSSSSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDWEWWKFKKDKKDSKAKLSLDKSKALLLLLLLLLLRTRTETWTWWWWWWWWWWOOOOOOO";
        char[] array = input.toCharArray();
        for(int code: array){
            codes[code].first++;
            codes[code].second=code;
        }
        bubbleSort(codes);
        //Arrays.sort(codes);
        int i = 0;
        int[][] tmp = new int[10][2];
        boolean[][] matrix = creatMatrix(bubbleSort(codes));
//        char ch  = 'a';
//        System.out.println((int)ch);
    }
    public static Elem[] bubbleSort(Elem[] array){
        boolean sorted = false;
        while (!sorted){
            sorted = true;
            for (int i = 0; i < array.length - 1; i ++){
                if (array[i].first < array[i + 1].first){
                    Elem j = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = j;
                    sorted = false;
                }
            }
        }
        return array;
    }

    public static boolean[][] creatMatrix(Elem array[]){
        double tmp = array[0].first / 10.0;
        String[][] lines = new String[10][10];
        for(int i = 0; i < 10; i++){
            for(int i1 = 0; i1 < 10; i1++){
                lines[i][i1] = "";
            }
        }
        for(int i = 0; i < 10; i++){
            Elem elem = array[i];
            if(elem.first < 10){
                System.out.print(elem.first + "  ");
            }
            else{
                System.out.print(elem.first + " ");
            }
        }
        System.out.println();
        //System.out.println(String.valueOf(array[0].first) + " " + String.valueOf(array[1].first) + " " + String.valueOf(array[2].first) + " " + String.valueOf(array[3].first) + " " + String.valueOf(array[4].first) + " " + String.valueOf(array[5].first) + " " + String.valueOf(array[6].first) + " " + String.valueOf(array[7].first) + " " + String.valueOf(array[8].first) + " " + String.valueOf(array[9].first));
        lines[0][0] = "#  ";
        lines[0][1] = "#  ";
        lines[0][2] = "#  ";
        lines[0][3] = "#  ";
        lines[0][4] = "#  ";
        lines[0][5] = "#  ";
        lines[0][6] = "#  ";
        lines[0][7] = "#  ";
        lines[0][8] = "#  ";
        lines[0][9] = "#  ";
        for(int i = 1; i < 10; i++){
            int i2 = (int) (10 - (array[i].first / tmp));
            int i1 = 9;
            for(; i1 > i2; i1--){
                lines[i][i1] += "#  ";
            }
        }
        for(int i = 0; i < 10; i++){
            lines[0][i] += "\n";
        }
        for(int i = 1; i < 10; i++){
            for(int i1 = 0; i1 < 10; i1++){
                System.out.print(lines[i1][i]);
            }
        }
        System.out.println("");
//        for(String str: lines){
//            System.out.println(str);
//        }
        System.out.println((char)array[0].second + "  " + (char)array[1].second + "  " + (char)array[2].second + "  " + (char)array[3].second + "  " + (char)array[4].second + "  " + (char)array[5].second + "  " + (char)array[6].second + "  " + (char)array[7].second + "  " + (char)array[8].second + "  " + (char)array[9].second);
        return null;
    }
}