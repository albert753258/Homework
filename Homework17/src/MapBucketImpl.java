public class MapBucketImpl<K, V>{
    private static final int DEFAULT_SIZE = 16;
    MapEntry<K, V> entries[];

    public MapBucketImpl() {
        entries = new MapEntry[DEFAULT_SIZE];
    }

    public MapEntry<K, V> get(K key) {
        int hashCode = key.hashCode();
        int position = hashCode & (entries.length - 1);
        MapEntry<K, V> current = this.entries[position];
        while (current != null){
            if(current.key.equals(key)){
                return current;
            }
            current = current.next;
        }
        return null;
    }
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int hashCode = key.hashCode();
        int position = hashCode & (entries.length - 1);
        if (this.entries[position] != null) {
            MapEntry<K, V> current = this.entries[position];
            while (current != null) {
                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                current = current.next;
            }
            // теперь новый элемент указывает на самый первый элемент в bucket
            newMapEntry.next = entries[position];
            // теперь новый элемент становится самым первым в bucket
            entries[position] = newMapEntry;
        }
        entries[position] = newMapEntry;
    }

    static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
