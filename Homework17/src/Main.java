public class Main {

    public static void main(String[] args) {
        MapBucketImpl<String, Integer> agesMap = new MapBucketImpl<>();
        agesMap.put("Марсель", 26);
        agesMap.put("Айдар", 25);
        agesMap.put("Альберт", 13);
        agesMap.put("Александр", 22);
        agesMap.put("Олег", 28);
        agesMap.put("Шамиль", 28);
        agesMap.put("Виктор", 23);
        agesMap.put("Алия", 19);
        agesMap.put("Марсель", 27);

        System.out.println(agesMap.get("Марсель").value);
        System.out.println(agesMap.get("Айдар").value);
        System.out.println(agesMap.get("Альберт").value);
        System.out.println(agesMap.get("Александр").value);
        System.out.println(agesMap.get("Олег").value);
        System.out.println(agesMap.get("Шамиль").value);
        System.out.println(agesMap.get("Виктор").value);
        System.out.println(agesMap.get("Алия").value);
    }
}
