import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(fib(0, 1, sc.nextLong()));

    }
    public static long fib(long x1, long x2, long n){
        long x3;
        if(n > 1){
            x3 = x1 + x2;
            x1 = x2;
            x2= x3;
            return fib(x1, x2, n - 1);
        }
        else{
            return x2;
        }
    }
}
