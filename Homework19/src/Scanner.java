import java.io.*;

public class Scanner {
    private InputStream scanner;
    public String nextLine(String file){
        try {
            scanner = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
        String str = "";
        int i = 0;
        while((char)i != '\n' && i != -1){
            try {
                i = scanner.read();
            }
            catch (IOException e) {
                throw new IllegalStateException(e);
            }
            if(i != -1){
                str = str + (char)i;
            }
        }
        return str;
    }
    public String nextInt(String file){
        try {
            scanner = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String str = "";
        int i = 39;
        while((char)i != ' ' && (char)i != '\n' && i != -1){
            try {
                i = scanner.read();
            }
            catch (IOException e) {
            }
            if((char)i >= '0' && (char)i <= '9'){
                str = str + (char)i;
            }
        }
        return str;
    }
}
