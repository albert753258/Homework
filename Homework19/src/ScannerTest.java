import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ScannerTest {
    Scanner scanner;
    @Before
    public void setUp() {
        scanner = new Scanner();
    }

    @Test
    public void testNextLine() {
        boolean actual = "21642378523415ewgerwgwghtrheyrsjstudytsutdyjt".equals(scanner.nextLine("text.txt"));
        assertTrue(actual);
    }
    @Test
    public void testNextInt() {
        boolean actual = "132434243".equals(scanner.nextInt("numbers.txt"));
        assertTrue(actual);
    }
}
