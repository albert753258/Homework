import java.io.*;

public class Scanner {
    private InputStream scanner;
    public String nextLine(){
        try {
            scanner = new FileInputStream("text.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String str = "";
        int i = 0;
        while((char)i != '\n'){
            try {
                i = scanner.read();
            }
            catch (IOException e) {
            }
            str = str + (char)i;
        }
        return str;
    }
    public String nextInt(){
        try {
            scanner = new FileInputStream("text.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String str = "";
        int i = 39;
        while((char)i != ' ' && (char)i != '\n'){
            try {
                i = scanner.read();
            }
            catch (IOException e) {
            }
            if((char)i >= '0' && (char)i <= '9'){
                str = str + (char)i;
            }
        }
        return str;
    }
}
