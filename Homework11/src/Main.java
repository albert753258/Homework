import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        char a = 'z';
//        int b = a;
//        System.out.println(b);
        Scanner scanner = new Scanner(System.in);
        char text[] = scanner.nextLine().toCharArray();
//        int array1[] = bubbleSort(analyze(text));
//        for(int i = 0; i < array1.length; i++){
//            System.out.println(array1[i]);
//        }
        findMax(analyze(text), bubbleSort(analyze(text)));
    }
    public static int[] analyze(char text[]){
        int countArray[] = new int[52];
        for(int i = 0; i < text.length; i ++){
            if(65<= text[i] & text[i] <= 90){
                int temp = text[i] - 65;
                countArray[temp] ++;
            }
            if(97<= text[i] & text[i] <= 122){
                int temp = text[i] - 97;
                countArray[temp] ++;
            }
        }
        return countArray;
    }
    public static int[] bubbleSort(int array[]){
        boolean sorted = false;
        int j;
        while (!sorted){
            sorted = true;
            for (int i = 0; i < array.length - 1; i ++){
                if (array[i] < array[i + 1]){
                    j = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = j;
                    sorted = false;
                }
            }
        }
        return array;
    }
    public static void findMax(int array[], int countArray[]){
        for (int i = 0; i < array.length; i ++){
            if (array[0] == countArray[i]){
                System.out.println((char) (i + 97) + " - " + array[i]);
            }
        }
    }
}