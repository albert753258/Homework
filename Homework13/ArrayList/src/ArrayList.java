public class ArrayList<E> {
    public class Iterator{
        int i = 0;
        E current = elements[i];
        public boolean hasNext() {
            return current != null;
        }

        public E next() {
            i++;
            E value = (E) current;
            current = elements[i];
            return value;
        }
    }
    private static final int DEFAULT_ARRAY_SIZE = 10;

    E[] elements;
    private int count;

    public ArrayList() {
        elements =  (E[])new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public void add(E element) {
        if (count < elements.length) {
            elements[count] = element;
            this.count++;
        } else {
            E[] temp = (E[])new Object[(int) (elements.length * 1.5)];
            for(int i = 0; i < elements.length; i ++){
                temp[i] = elements[i];
            }
            elements = (E[])new Object[(int) (elements.length * 1.5)];
            for(int i = 0; i < temp.length; i ++){
                elements[i] = temp[i];
            }
            add(element);
        }
    }

    public E get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Неверный индекс");
            return null;
        }
    }

    public void addToBegin(E element) {
        if (count < DEFAULT_ARRAY_SIZE) {
            for (int i = count; i > 0; i--) {
                elements[i] = elements[i - 1];
            }
            this.elements[0] = element;
            this.count++;
        } else {
            E[] temp = (E[])new Object[(int) (elements.length * 1.5)];
            for(int i = 0; i < elements.length; i ++){
                temp[i] = elements[i];
            }
            elements = (E[])new Object[(int) (elements.length * 1.5)];
            for(int i = 0; i < temp.length; i ++) {
                elements[i] = temp[i];
            }
            addToBegin(element);
        }
    }

    public void remove(E element) {
        int index = contains(element);
        if(index != -1){
            removeByIndex(index);
        }
    }

    public void removeByIndex(int index) {
        for(int i = index; i < elements.length - 1; i ++){
            elements[i] = elements[i + 1];
        }
        count--;
    }

    public int contains(E element) {
        for (int i = 0; i < elements.length; i ++){
            if(elements[i] == element){
                return i;
            }
        }
        return -1;
    }

    public int size() {
        return count;
    }
    void initializeArray(){

    }
}
