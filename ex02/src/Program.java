import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int coffee = 0;
        int num = scanner.nextInt();
        while(num != 42){
            int sum = 0;
            while(num != 0){
                sum += num % 10;
                num /= 10;
            }
            if(isPrime(sum)){
                coffee++;
            }
            num = scanner.nextInt();
        }
        System.out.println("Count of coffee-request - " + coffee);
    }
    public static boolean isPrime(int num){
        if(num <= 1){
            return true;
        }
        for(int i = 2; i <= Math.sqrt(num); i++){
            if(num % i == 0){
                return false;
            }
        }
        return true;
    }
}
