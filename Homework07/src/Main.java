import java.util.Scanner;

public class Main {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println(sum());
        System.out.println(expand());
        System.out.println(aToB());

    }

    private static int aToB() {
        int a = sc.nextInt();
        int b = sc.nextInt();
        int sum = 0;
        for(int i = a; i <= b; i ++){
            sum = sum + i;
        }
        return sum;
    }

    private static int expand() {
        int number = sc.nextInt();
        int expanded = 0;
        while (number != 0){
            expanded = expanded * 10;
            expanded = expanded + number % 10;
            number = number / 10;
        }
        return expanded;
    }

    public static int sum(){
        int sum = 0;
        for(int number = sc.nextInt(); number > 0; number /= 10){
            sum = sum + number % 10;
        }
        return sum;
    }

}
