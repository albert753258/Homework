import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char number[] = sc.nextLine().toCharArray();
        System.out.println(parseInt(number));
    }
    public static int parseInt(char number[]) {
        int number1 = 0;
        int length = number.length - 1;
        for (int i = 0; i < number.length; i ++) {
            number1 = number1 + degree(number[i] - 48, length);
            length--;
        }

        return number1;
    }
    public static int  degree(int a, int b){
        while(b > 0){
            a = a * 10;
            b --;
        }
        return a;
    }
}
