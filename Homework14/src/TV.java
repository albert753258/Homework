public class TV {
    Channel[] channels = new Channel[4];
    Controller controller;

    public void show(int channelNumber, int time) {
        channels[channelNumber].play(time);
    }
    {
        Channel firstChannel = new Channel("1 Channel");
        Channel ntv = new Channel("NTV");
        Channel russia1 = new Channel("Russia 1");
        Channel discovery = new Channel("Discovery");
        channels[0] = firstChannel;
        channels[1] = ntv;
        channels[2] = russia1;
        channels[3] = discovery;
    }
    public void setController(Controller controller){
        this.controller = controller;
        controller.tv = this;
    }
}
