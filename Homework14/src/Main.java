import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TV tv = new TV();
        Controller controller = new Controller();
        tv.setController(controller);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number of a channel");
        int number = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter time");
        int time = Integer.parseInt(scanner.nextLine());
        controller.show(number,time);
    }
}
