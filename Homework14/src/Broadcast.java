public class Broadcast {
    int timeOfStart;
    int timeOfEnd;
    String name;
    public Broadcast(String name, int timeOfStart, int timeOfEnd){
        this.name = name;
        this.timeOfStart = timeOfStart;
        this.timeOfEnd = timeOfEnd;
    }

    public String getName() {
        return name.toString();
    }
}
