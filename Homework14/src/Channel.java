public class Channel {
    int number;
    String name;
    Broadcast[] broadcasts = new Broadcast[3];
    {
        Broadcast broadcast1 = new Broadcast("The thirst broadkast", 0, 8);
        Broadcast broadcast2 = new Broadcast("The second broadcast", 8, 16);
        Broadcast broadcast3 = new Broadcast("The third broadcast", 16, 24);
        broadcasts[0] = broadcast1;
        broadcasts[1] = broadcast2;
        broadcasts[2] = broadcast3;
    }
    public Channel(String name){
        this.name = name;
    }

    public void play(int time) {
        String broadcastName = "Error";
        for(Broadcast broadcast : broadcasts){
            if(broadcast.timeOfStart <= time & time <= broadcast.timeOfEnd){
                broadcastName = broadcast.getName();
                break;
            }
        }
        System.out.println("You are watching a broadcast " + broadcastName + " on channel " + name + ".");
    }
}
