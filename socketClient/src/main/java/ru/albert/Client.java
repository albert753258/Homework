package ru.albert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private Socket client;
    private PrintWriter toServer;
    private BufferedReader fromServer;
    public Client(String host, int port){
        try {
            client = new Socket(host, port);
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiver).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
    public void send(String message){
        toServer.println(message);
    }
    private Runnable receiver = new Runnable() {
        public void run() {
            try {
                while (true){
                    String messageFromServer = fromServer.readLine();
                    System.out.println(messageFromServer);
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    };
}
