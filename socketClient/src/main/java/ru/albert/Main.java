package ru.albert;

import com.beust.jcommander.JCommander;

import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Arguments arguments = new Arguments();
        JCommander.newBuilder().addObject(arguments).build().parse(args);

        Client client = new Client(arguments.host, arguments.port);
        while(true){
            String message = sc.nextLine();
            client.send(message);
        }
    }
}
