package ru.albert;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++){
            File myFolder = new File(args[i]);
            File[] files = myFolder.listFiles();
            for (int t = 0; t < files.length; t++){
                System.out.println(files[t].getName() + "     " + files[t].length());
            }
            System.out.println('\n');
        }
    }
}