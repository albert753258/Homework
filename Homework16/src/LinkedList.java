public class LinkedList<E> implements List<E>{
    public class Node<E> {
        private E value;
        private Node next;

        public Node(E value) {
            this.value = value;
        }

        public E getValue() {
            return value;
        }

        public void setValue(E value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }
    public class LinkedListIterator {

        private Node current;

        public LinkedListIterator() {
            current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public E next() {
            E value = (E) current.getValue();
            current = current.getNext();
            return value;
        }
    }
    public LinkedListIterator iterator = new LinkedListIterator();
    Node first;
    private Node last;

    private int count;

    public LinkedList() {

    }
    @Override
    public void add(E element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }
    @Override
    public E get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return (E) current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return null;
        }
    }
    @Override
    public void addToBegin(E element) {
        Node node = new Node(element);
        node.setNext(first);
        first = node;
    }
    @Override
    public void remove(E element) {
        int index = contains(element);
        if(index != -1){
            removeByIndex(index);
        }
    }
    @Override
    public void removeByIndex(int index) {
        Node node = first;
        for(int i = 1; i < index; i++){
            node = node.getNext();
        }
        node.setNext(node.getNext().getNext());
        count--;
    }
    @Override
    public int contains(E element) {
        Node node = first;
        for(int i = 0; i < count; i++){
            if(node.getValue() == element){
                return i;
            }
            node = node.getNext();
        }
        return -1;
    }
    @Override
    public int size() {
        return count;
    }
}