public interface List<E> {
    public void add(E element);
    public E get(int index);
    public void addToBegin(E element);
    public void remove(E element);
    public void removeByIndex(int index);
    public int contains(E element);
    public int size();
}
