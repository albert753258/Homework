import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int iterations = 0;
        int num = scanner.nextInt();
        if(num <= 1){
            throw new IllegalArgumentException();
        }
        int sqrt = (int) Math.sqrt(num);
        for(int i = 2; i <= sqrt; i++){
            iterations++;
            if(num % i == 0){
                System.out.println("false " + iterations);
                return;
            }
        }
        iterations++;
        System.out.println("true " + iterations);
    }
}
